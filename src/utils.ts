import { getCurrentInstance } from "@vue/runtime-core";

export function vm() {
  // eslint-disable-next-line
  const instance = getCurrentInstance()!;
  return instance;
}